package routines;
import java.util.ArrayList;

public class FixedLengthFileUtil {

	
    public static String convertToFixedLength(String line, int[][] positionBaseInfos) {
        StringBuilder sb = new StringBuilder();
        for (int[] positionBaseInfo : positionBaseInfos) {
        	sb.append(line.substring(positionBaseInfo[0], positionBaseInfo[1]));
        }
        return sb.toString();
    }
    
    /**
     * Input positions are half closed interval. Left is inclusive, right is exclusive
     * @param line
     * @param positionBaseInfos
     * @param delimiter
     * @return
     */
    public static String convertToFixedLength(String line, int[][] positionBaseInfos, String delimiter) {
        ArrayList<String> parts = new ArrayList<>();
        for(int[] positionBaseInfo: positionBaseInfos) {	
            if(positionBaseInfo[1] > line.length()) {
                parts.add("");
            } else {
                parts.add(line.substring(positionBaseInfo[0], positionBaseInfo[1]));
            }
        }
        return String.join(delimiter, parts);
    }
    
    /**
     * Input positions are closed at both end and counting from 1.
     * We convert it to Java convention, that is intervals are half closed, including left end and excluding right end point. And all positions
     * start from 0.
     * @param columns_length
     * @return
     */
	public static int[][] getPosition(String columns_length) {
		 int[][] positionBasedInfos = null;
		  	String[] allColInfos = columns_length.split(",");
		  	positionBasedInfos = new int[allColInfos.length][];
		  	int i = 0;
		  	for (String colInfos : allColInfos) {
		  		String[] colInfo = colInfos.split("-");
		  		positionBasedInfos[i] = new int[] {Integer.parseInt(colInfo[0]) - 1, Integer.parseInt(colInfo[1])};
		  		i++;
		  	}
		  return positionBasedInfos;
	}
	
	public static int getTotalColumnLen(int[][] positionBasedInfos) {

		  	int totalLen = 0;
		  	for (int [] colInfos : positionBasedInfos) {
		  		totalLen = totalLen + (colInfos[1] - colInfos[0]);		  		
		  	}
		  return totalLen;
	}
	
	/**
	 * The position intervals are half closed. So the actual length is just X.
	 * @param positionBasedInfos
	 * @return
	 */
	public static int getLastColumnLen(int[][] positionBasedInfos) {
		return positionBasedInfos[positionBasedInfos.length-1][1];
	}
}