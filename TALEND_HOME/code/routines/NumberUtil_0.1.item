package routines;

import org.apache.commons.lang.StringUtils;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class NumberUtil {

    /**
     * formatNumberTwoDecimals: Return the number string formated to two decimal places
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("number") input: The string need to be printed.
     * {param} int(2) input: Number of decimal places to format
     * 
     * {example} formatNumberTwoDecimals("123.4", 2) # 123.40.
     */
    public static String formatNumberWithDecimals(String message, int decimals) {
        if ( StringUtils.isBlank(message) ) 
        	return null;
        
        Float f = Float.parseFloat(message);
        return String.format("%." + decimals + "f", f);
    }
}
